'use strict';
var express = require('express'),
  casinosService = require('../services/casinos.service'),
  router = express.Router();

router.post('/', casinosService.getCasinos);
router.post('/menus', casinosService.getMenus);

module.exports = router;