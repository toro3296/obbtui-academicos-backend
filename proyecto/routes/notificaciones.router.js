'use strict';
var express = require('express'),
  notificacionesService = require('../services/notificaciones.service'),
  router = express.Router();

router.post('/', notificacionesService.getNotificaciones);
router.post('/tipos', notificacionesService.getTipos);
router.post('/enviar', notificacionesService.sendNotificacion);

module.exports = router;