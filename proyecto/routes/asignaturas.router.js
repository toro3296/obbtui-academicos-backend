'use strict';
var express = require('express'),
  asignaturasService = require('../services/asignaturas.service'),
  router = express.Router();

router.post('/', asignaturasService.getAsignaturas);
router.post('/estudiantes', asignaturasService.getEstudiantes);
router.post('/correo', asignaturasService.sendCorreo);

module.exports = router;