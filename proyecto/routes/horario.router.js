'use strict';
var express = require('express'),
  horarioService = require('../services/horario.service'),
  router = express.Router();

router.post('/', horarioService.getHorario);
router.post('/bloques', horarioService.getBloques);

module.exports = router;