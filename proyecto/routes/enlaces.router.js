'use strict';
var express = require('express'),
  enlacesService = require('../services/enlaces.service'),
  router = express.Router();

router.post('/', enlacesService.getEnlaces);

module.exports = router;