'use strict';
var reply = require('../../base/utils/reply');

function getCasinos(request, response) {
  try {
    // Obtener los casinos
    let casinos = [
      {
        cod_casino: 1,
        nombre: 'Humanidades',
        latitud: '-33.0507066',
        longitud: '-71.6312547'
      },
      {
        cod_casino: 2,
        nombre: 'Odontología',
        latitud: '-33.005524',
        longitud: '-71.5652749'
      },
      {
        cod_casino: 3,
        nombre: 'Santiago',
        latitud: '-32.9337749',
        longitud: '-71.5393166'
      }
    ];

    response.json(reply.ok(casinos));
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

function getMenus(request, response) {
  try {
    var args = JSON.parse(request.body.arg === undefined ? '{}' : request.body.arg),
      propCasino = args.hasOwnProperty('casino'),
      propFechaI = args.hasOwnProperty('fecha_inicio'), // Variable para agregar filtro por fecha seleccionada
      propFechaF = args.hasOwnProperty('fecha_fin'); // Variable para agregar filtro por fecha seleccionada

    if(!propCasino) {
      response.json(reply.error('Casino no seleccinado'));
    } else if(parseInt(args.casino) < 1 || parseInt(args.casino) > 3) {
      response.json(reply.error('Casino no válido'));
    } else {
      // Obtener los menús de cada casino
      let id = args.casino;
      let casinos = [
        [
          {
            categoria: 'GENERAL',
            menu: 'FIDEOS CON SALSA BOLOGNESA'
          },
          {
            categoria: 'VEGETARIANO',
            menu: 'ENSALADA CÉSAR'
          }
        ],
        [
          {
            categoria: 'GENERAL',
            menu: 'PAPAS FRITAS CON ARROZ'
          },
          {
            categoria: 'VEGETARIANO',
            menu: 'ENSALADA CÉSAR CON TOMATES CHERRY'
          }

        ],
        [
          {
            categoria: 'GENERAL',
            menu: 'BARROS LUCO CON COCA-COLA LIGHT'
          },
          {
            categoria: 'VEGETARIANO',
            menu: 'SHAWARMA DE LECHUGA'
          }
        ]
      ];

      response.json(reply.ok(casinos[ id - 1 ]));
    }
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

module.exports = {
  getCasinos,
  getMenus
};