'use strict';
var reply = require('../../base/utils/reply');

function getAsignaturas(request, response) {
  try {
    // Obtener la lista de asignaturas del académico
    let asignaturas = [
      {
        codigo: 'INC100',
        nombre: 'CÁLCULO I',
        carrera: 'INGENIERIA CIVIL INFORMÁTICA'
      }
    ];

    response.json(reply.ok(asignaturas));
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

function getEstudiantes(request, response) {
  try {
    // Obtener la lista de estudiantes de la asignatura
    let estudiantes = [
      {
        nombre: 'CAMILO RAVELO',
        run: '11111111',
        correo: 'camilo.ravelo@uv.cl'
      },
      {
        nombre: 'OMAR CIFUENTES',
        run: '22222222',
        correo: 'omar.cifuentes@uv.cl'
      },
      {
        nombre: 'JORGE GARÍN',
        run: '33333333',
        correo: 'jorge.garinro@uv.cl'
      }
    ];

    response.json(reply.ok(estudiantes));
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

function sendCorreo(request, response) {
  try {
    // Enviar correo a los estudiantes
    var args = JSON.parse(request.body.arg === undefined ? '{}' : request.body.arg),
      propAsunto = args.hasOwnProperty('asunto'),
      propMensaje = args.hasOwnProperty('mensaje'),
      propEstudiantes = args.hasOwnProperty('estudiantes');

    if(!propAsunto) {
      response.json(reply.error('Parámetro "asunto" no encontrado'));
    } else if(!propMensaje) {
      response.json(reply.error('Parámetro "mensaje" no encontrado'));
    } else if(!propEstudiantes) {
      response.json(reply.error('Parámetro "estudiantes" no encontrado'));
    } else {
      // Enviar notificación a OneSignal
      freeze(4);
      response.json(reply.ok('Notificación enviada satisfactoriamente'));
    }
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

function freeze(time) {
  const stop = new Date().getTime() + time * 1000;
  while(new Date().getTime() < stop);
}

module.exports = {
  getAsignaturas,
  getEstudiantes,
  sendCorreo
};