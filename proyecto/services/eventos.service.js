'use strict';
var reply = require('../../base/utils/reply');

function getEventos(request, response) {
  try {
    // Obtener la lista de eventos
    let eventos = {
      hoy: [
        {
          titulo: 'Evento de hoy',
          imagen: 'https://i.imgur.com/EZDqCq3.png',
          lugar: 'Somewhere in the time',
          cuerpo: 'Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada.',
          fecha: {
            dia: '08',
            mes_nombre: 'AGO',
            tiempo: '18:00',
            completa: '08 AGO, a las 18:00 horas'
          }
        }
      ],
      proximos: [
        {
          titulo: 'Evento próximo 01',
          imagen: 'https://i.imgur.com/EZDqCq3.png',
          lugar: 'Somewhere in the time',
          cuerpo: 'Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada.',
          fecha: {
            dia: '09',
            mes_nombre: 'AGO',
            tiempo: '18:00',
            completa: '09 AGO, a las 18:00 horas'
          }
        },
        {
          titulo: 'Evento próximo 02',
          imagen: 'https://i.imgur.com/EZDqCq3.png',
          lugar: 'Somewhere in the time 2',
          cuerpo: 'Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada.',
          fecha: {
            dia: '10',
            mes_nombre: 'AGO',
            tiempo: '18:00',
            completa: '10 AGO, a las 18:00 horas'
          }
        }
        ]
    };

    response.json(reply.ok(eventos));
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

function strDate(number) {
  return ('0' + number).substr(-2);
}

module.exports = {
  getEventos
};